<h1>CodeChef Exercises</h1>
<p>
This folder contains some simple exercises from <a href="https://www.codechef.com">CodeChef</a>, an educational platform for the programming community.
</p>
