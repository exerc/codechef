package com.vaccine.main;

import java.io.*;

//Aufgabenstellung: https://www.codechef.com/status/VACCINE2

class CodeChefVaccine2 {

	static PrintWriter output;
	static InputReader input;
	
	final int dmax = (int)1e5;
	final int tmax = 10;
	final int nmax = (int)1e4;
	
	public static void main (String[] args) throws IOException {
		output = new PrintWriter(System.out);
		// output = new PrintWriter(new FileWriter("Filepath/output.txt"));
		input = new InputReader();
		new CodeChefVaccine2();
		output.flush();
		output.close();
	}
	
	CodeChefVaccine2() throws IOException {
		solve();
	}
		
	void solve() {
		int t = input.nextInt();
		while (t-- > 0) {
			int n = input.nextInt(), d = input.nextInt();
			int risk = 0, noRisk = 0;
			for (int i = 0; i < n; i++) {
				int c = input.nextInt();
				if (c <= 9 || c >= 80) {
					risk++;
					} else {
					noRisk++;
				}
			}
			if (risk + noRisk != n || n > nmax || d > dmax || t > tmax || t < 0 || n < 0 || d < 0)
				System.out.print("Error in testcase: " + (t + 1));				
				System.out.println(((risk + d - 1) / d + (noRisk + d - 1) / d));
		}
	}
}
