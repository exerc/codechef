package com.vaccine.main;

import java.io.File;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class InputReader {
	BufferedReader br;
	StringTokenizer st;
	File file = new File("inputVaccine.txt");

	InputReader() throws IOException  {
		br = new BufferedReader(new FileReader(file));
		// br = new BufferedReader(new InputStreamReader(System.in));
	}
	
	public String next() {
		while (st == null || !st.hasMoreTokens()) {
			try {
				st = new StringTokenizer(br.readLine());
			} 
			catch (IOException e) {
			}
		}
		return st.nextToken();
	}
	
	public int nextInt() {
		return Integer.parseInt(next());
	}

}
