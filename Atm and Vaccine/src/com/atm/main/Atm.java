package com.atm.main;

import java.io.*;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

//Aufgabenstellung: https://www.codechef.com/problems/HS08TEST

public class Atm {
	
	static InputReader input;
	static PrintWriter output;
	
	final int withdrawMax = 2000;
	final double initAccBalMax = 2000;

	public static void main (String[] args) throws IOException{
		
		input = new InputReader();
		output = new PrintWriter(System.out);
		
		new Atm();
		output.flush();
		output.close();
	}
	
	Atm() {
		checkBalance();
	}
	
		// Auf Konsoleninput warten. 
		// Erster Input ist Hoehe des Abhebebetrags, zweiter das Guthaben des Kontos.
	void checkBalance() {
		
		final int scale = 2;
		int withdraw = input.nextInt();
		double accBal = input.nextDouble();
		double fee = 0.5;
		BigDecimal tempBig;
		
		// Abhebevorgang erlaubt nur Fuenfer-Betraege.
		if (withdraw%5 == 0 && accBal - withdraw >= 0.5 && withdraw <= withdrawMax && accBal <= initAccBalMax && withdraw > 0 && accBal > 0) {
			tempBig = new BigDecimal(accBal - withdraw - fee).setScale(scale, RoundingMode.HALF_EVEN);	
		} else {
			tempBig = new BigDecimal(accBal).setScale(scale, RoundingMode.HALF_EVEN);
		}
		output.println(tempBig);
	}
	
	public static class InputReader {
		BufferedReader br;
		StringTokenizer st;
		
		InputReader() throws IOException {
			br = new BufferedReader(new InputStreamReader(System.in));
		}
		
		public String next() {
			while (st == null || !st.hasMoreTokens()) {
				try {
					st = new StringTokenizer(br.readLine());
				}
				catch (IOException e) {	
				}
			}
			return st.nextToken();	
		}
		
		public double nextDouble() {
			return Double.parseDouble(next());
		}
		
		public int nextInt() {
			return Integer.parseInt(next());
		}
	}
}
